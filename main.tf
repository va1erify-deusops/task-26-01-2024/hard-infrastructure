module "network" {
  source = "https://gitlab.com/api/v4/projects/56150052/packages/terraform/modules/yandex-vpc-network/yandex-cloud/0.2.0"
  name   = "default-network"
}

module "subnetwork" {
  source     = "https://gitlab.com/api/v4/projects/56150052/packages/terraform/modules/vps-subnet/yandex-cloud/0.2.0"
  id         = module.network.id
  cidr_v4    = "10.10.10.0/24"
  depends_on = [module.network]
  zone       = var.zone
  name       = "default-subnetwork"
}


module "security_group" {
  source                    = "https://gitlab.com/api/v4/projects/56150052/packages/terraform/modules/yandex-vpc-security-group/yandex-cloud/0.2.0"
  network_id                = module.network.id
  subnetwork_v4_cidr_blocks = module.subnetwork.v4_cidr_blocks
}

module "k8_service_account" {
  source    = "https://gitlab.com/api/v4/projects/56150052/packages/terraform/modules/yandex-iam-service-account/yandex-cloud/0.2.0"
  folder_id = var.folder_id
  name      = "k8-service-account"
}


module "folder_iam_binding_admin" {
  source       = "https://gitlab.com/api/v4/projects/56150052/packages/terraform/modules/yandex-resourcemanager-folder-iam-binding/yandex-cloud/0.1.0"
  folder_id    = var.folder_id
  role         = "admin"
  members_list = ["serviceAccount:${module.k8_service_account.id}"]
}

module "folder_iam_binding_editor" {
  source       = "https://gitlab.com/api/v4/projects/56150052/packages/terraform/modules/yandex-resourcemanager-folder-iam-binding/yandex-cloud/0.1.0"
  folder_id    = var.folder_id
  role         = "editor"
  members_list = ["serviceAccount:${module.k8_service_account.id}"]
}


module "folder_iam_member_cluster_agent_role_for_SA" {
  source       = "https://gitlab.com/api/v4/projects/56150052/packages/terraform/modules/yandex-resourcemanager-folder-iam-member/yandex-cloud/0.2.0"
  folder_id    = var.folder_id
  role         = "k8s.clusters.agent"
  members_list = "serviceAccount:${module.k8_service_account.id}"
}

module "folder_iam_member_vpc_public_admin_role_for_SA" {
  source       = "https://gitlab.com/api/v4/projects/56150052/packages/terraform/modules/yandex-resourcemanager-folder-iam-member/yandex-cloud/0.2.0"
  folder_id    = var.folder_id
  role         = "vpc.publicAdmin"
  members_list = "serviceAccount:${module.k8_service_account.id}"
}

module "folder_iam_member_container_registry_images_puller_role_for_SA" {
  source       = "https://gitlab.com/api/v4/projects/56150052/packages/terraform/modules/yandex-resourcemanager-folder-iam-member/yandex-cloud/0.2.0"
  folder_id    = var.folder_id
  role         = "container-registry.images.puller"
  members_list = "serviceAccount:${module.k8_service_account.id}"
}

module "folder_iam_member_kms_keys_encrypter_decrypter_role_for_SA" {
  source       = "https://gitlab.com/api/v4/projects/56150052/packages/terraform/modules/yandex-resourcemanager-folder-iam-member/yandex-cloud/0.2.0"
  folder_id    = var.folder_id
  role         = "kms.keys.encrypterDecrypter"
  members_list = "serviceAccount:${module.k8_service_account.id}"
}

module "kms_symmetric_key" {
  source            = "https://gitlab.com/api/v4/projects/56150052/packages/terraform/modules/yandex-kms-symmetric-key/yandex-cloud/0.1.0"
  name              = "default_kms_key_name"
  default_algorithm = "AES_128"
  rotation_period   = "8760h"
}

module "kubernetes_cluster" {
  source       = "https://gitlab.com/api/v4/projects/56150052/packages/terraform/modules/yandex-kubernetes-cluster/yandex-cloud/0.3.0"
  cluster_name = "k8s-cluster"
  network_id   = module.network.id

  subnetwork_id   = module.subnetwork.id
  subnetwork_zone = module.subnetwork.zone

  cluster_public_ip = "true"

  vpc_security_group_id = [module.security_group.id]

  k8_node_service_account = module.k8_service_account
  k8_service_account      = module.k8_service_account

  depends_on = [
    module.folder_iam_member_cluster_agent_role_for_SA,
    module.folder_iam_member_container_registry_images_puller_role_for_SA,
    module.folder_iam_member_kms_keys_encrypter_decrypter_role_for_SA,
    module.folder_iam_member_vpc_public_admin_role_for_SA
  ]

  master_version = "1.28"
}

module "kubernetes_node_group" {
  source     = "https://gitlab.com/api/v4/projects/56150052/packages/terraform/modules/yandex-kubernetes-node-group/yandex-cloud/0.2.0"
  cluster_id = module.kubernetes_cluster.id
  name       = "k8s-node-group"
  labels     = {
    "app" = "2048"
  }
  platform_id = "standard-v3"
  nat         = "true"
  subnet_ids  = [
    module.subnetwork.id
  ]
  security_group_ids = [
    module.security_group.id
  ]
  memory                          = "16"
  cores                           = "8"
  core_fraction                   = "100"
  boot_disk_size                  = "30"
  boot_disk_type                  = "network-hdd"
  preemptible                     = false
  container_runtime_type          = "containerd"
  scale_policy_initial            = "1"
  scale_policy_min                = "1"
  scale_policy_max                = "2"
  zone                            = module.subnetwork.zone
  maintenance_policy_auto_upgrade = true
  maintenance_policy_auto_repair  = true
}






