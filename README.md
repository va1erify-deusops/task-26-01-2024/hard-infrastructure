# Инфраструктура для решения уровня Hard [Проекта](https://gitlab.com/va1erify-deusops/task-26-01-2024/about_project)

Этот репозиторий содержит конфигурации Terraform для развертывания инфраструктуры в Yandex Cloud с использованием Terraform модулей.

## Краткие тезисы

- Инфраструктура поднимается с помощью Terraform с использованием модулей для Yandex Cloud (версионированные модули берутся из отдельного репозитория).
- `main.tf` НЕ параметризирован для удобства просмотра всей конфигурации в одном файле.
- Хранение состояния осуществляется в Terraform State. Template для конфигурирования инклюдится из отдельного репозитория.
- Переменные для GitLab CI/CD берутся из отдельного репозитория.
- Шаги GitLab CI/CD включают:
  - `terraform fmt` (автоматический запуск)
  - `terraform validate` (автоматический запуск)
  - `build` (ручной запуск)
  - `deploy-infrastructure` (ручной запуск)
  - `cleanup` (ручной запуск, можно выполнить в любой момент)
- По результатам деплоя инфраструктуры генерируется файл `terraform_outputs.json` в виде артефакта.

## Создаваемые сущности

- `module "network"`: Создание сети.
- `module "subnetwork"`: Создание подсети.
- `module "security_group"`: Создание группы безопасности.
- `module "k8_service_account"`: Создание сервисного аккаунта.
- `module "folder_iam_binding_admin"`: Привязка роли администратора к папке.
- `module "folder_iam_binding_editor"`: Привязка роли редактора к папке.
- `module "folder_iam_member_cluster_agent_role_for_SA"`: Привязка роли агента кластера к сервисному аккаунту.
- `module "folder_iam_member_vpc_public_admin_role_for_SA"`: Привязка роли администратора VPC к сервисному аккаунту.
- `module "folder_iam_member_container_registry_images_puller_role_for_SA"`: Привязка роли для доступа к реестру контейнеров к сервисному аккаунту.
- `module "folder_iam_member_kms_keys_encrypter_decrypter_role_for_SA"`: Привязка роли для работы с ключами шифрования к сервисному аккаунту.
- `module "kms_symmetric_key"`: Создание симметричного ключа для шифрования.
- `module "kubernetes_cluster"`: Создание кластера Kubernetes.
- `module "kubernetes_node_group"`: Создание группы узлов Kubernetes.

## Используемые репозитории
- [CI template для использования Gitlab в качестве backend для Terraform](https://gitlab.com/va1erify-gitlab-ci/gitlab-terraform-ci)
- [Terraform модули для YC](https://gitlab.com/va1erify-terraform/yandex-modules/modules)
- [Variables для использования в gitlab-ci.yml](https://gitlab.com/va1erify-deusops/task-26-01-2024/hard-gitlab-variables)